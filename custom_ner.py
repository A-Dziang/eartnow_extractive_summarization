﻿# -*- coding: utf-8 -*-

import sys
import BeautifulSoup
import bs4
import argparse
import subprocess
import itertools
import re
import copy
import collections
import string
from nltk.stem.snowball import SnowballStemmer
import pickle
from collections import Iterable
from nltk.tag import ClassifierBasedTagger
from nltk.chunk import ChunkParserI
from nltk.chunk import conlltags2tree, tree2conlltags
import nltk
import itertools
import os
from summarizer_utils import *

FDIST = []
 
BEHEADED_FDIST = []


def features(tokens, index, history):
    """
    `tokens`  = a POS-tagged sentence [(w1, t1), ...]
    `index`   = the index of the token we want to extract features for
    `history` = the previous predicted IOB tags
    """
 
    # init the stemmer
    stemmer = SnowballStemmer('english')
 
    # Pad the sequence with placeholders
    tokens = [('[START2]', '[START2]'), ('[START1]', '[START1]')] + list(tokens) + [('[END1]', '[END1]'), ('[END2]', '[END2]')]
    history = ['[START2]', '[START1]'] + list(history)
 
    # shift the index with 2, to accommodate the padding
    index += 2
 
    word, pos = tokens[index]
    prevword, prevpos = tokens[index - 1]
    prevprevword, prevprevpos = tokens[index - 2]
    nextword, nextpos = tokens[index + 1]
    nextnextword, nextnextpos = tokens[index + 2]
    previob = history[index - 1]
    contains_dash = '-' in word
    contains_dot = '.' in word
    allascii = all([True for c in word if c in string.ascii_lowercase])
 
    allcaps = word == word.capitalize()
    capitalized = word[0] in string.ascii_uppercase
 
    prevallcaps = prevword == prevword.capitalize()
    prevcapitalized = prevword[0] in string.ascii_uppercase
 
    nextallcaps = prevword == prevword.capitalize()
    nextcapitalized = prevword[0] in string.ascii_uppercase
    wordlen = 1
    if ' ' in word:
        wordlen = word.split()
    cap_fdist = BEHEADED_FDIST[0][word.capitalize()]
    total_fdist = BEHEADED_FDIST[0][word.lower()] + BEHEADED_FDIST[0][word.capitalize()] + 0.0001
    
    fdist_cap_nocap = float(cap_fdist) / (total_fdist)

 
    return {
        'pos': pos,
        'all-ascii': allascii,
        'len': len(word),
        'wordlen': wordlen,
        'fdist_cap_nocap': fdist_cap_nocap,
 
        'next-lemma': stemmer.stem(nextword),
        'next-pos': nextpos,
        'nextlen': len(nextword),
 
        'next-next-word': nextnextword,
        'nextnextpos': nextnextpos,
        'nextnextlen': len(nextnextword),
 
        'prev-word': prevword,
        'prev-lemma': stemmer.stem(prevword),
        'prev-pos': prevpos,
        'prevlen': len(prevword),

        'prev-prev-word': prevprevword,
        'prev-prev-pos': prevprevpos,
        'nprevprevlen': len(prevprevword),
 
        'prev-iob': previob,
 
        'contains-dash': contains_dash,
        'contains-dot': contains_dot, 
        'apos': word.find('’'),

        'all-caps': allcaps,
        'capitalized': capitalized,
 
        'prev-all-caps': prevallcaps,
        'prev-capitalized': prevcapitalized,
 
        'next-all-caps': nextallcaps,
        'next-capitalized': nextcapitalized,
    }


def to_conll_iob(annotated_sentence):
    """
    `annotated_sentence` = list of triplets [(w1, t1, iob1), ...]
    Transform a pseudo-IOB notation: O, PERSON, PERSON, O, O, LOCATION, O
    to proper IOB notation: O, B-PERSON, I-PERSON, O, O, B-LOCATION, O
    """
    proper_iob_tokens = []
    for idx, annotated_token in enumerate(annotated_sentence):
        tag, word, ner = annotated_token
 
        if ner != 'O':
            if idx == 0:
                ner = "B-" + ner
            elif annotated_sentence[idx - 1][2] == ner:
                ner = "I-" + ner
            else:
                ner = "B-" + ner
        proper_iob_tokens.append((tag, word, ner))
    return proper_iob_tokens

def get_connl_form_token(annotated_token):

    annotations = annotated_token.split('\t')
    word, tag, ner = annotations[0], annotations[1], annotations[3]
 
    if ner != 'O':
        ner = ner.split('-')[0]
 
    if tag in ('LQU', 'RQU'):   # Make it NLTK compatible
        tag = "``"
    return word, tag, ner
 
 
def read_gmb(corpus_root):
    tokens = []
    paths = get_tag_file_paths(corpus_root)
    for path in paths: 
        with open(path, 'rb') as file_handle:
            file_content = file_handle.read().decode('utf-8').strip()
            annotated_sentences = file_content.split('\n\n')
            for annotated_sentence in annotated_sentences:
                annotated_tokens = [seq for seq in annotated_sentence.split('\n') if seq]
                standard_form_tokens = []
                for idx, annotated_token in enumerate(annotated_tokens):                
                    standard_form_tokens.append(get_connl_form_token(annotated_token))
 
                conll_tokens = to_conll_iob(standard_form_tokens)
 
                # Make it NLTK Classifier compatible - [(w1, t1, iob1), ...] to [((w1, t1), iob1), ...]
                # Because the classfier expects a tuple as input, first item input, second the class
                yield [((w, t), iob) for w, t, iob in conll_tokens]
 

def handle_tag_file(ner_tags, file_handle):
    file_content = file_handle.read().decode('utf-8').strip()
    annotated_sentences = file_content.split('\n\n')   # Split sentences
    for annotated_sentence in annotated_sentences:
        annotated_tokens = [seq for seq in annotated_sentence.split('\n') if seq]  # Split words
        standard_form_tokens = []
 
        for idx, annotated_token in enumerate(annotated_tokens):
             annotations = annotated_token.split('\t')   # Split annotation
             word, tag, ner = annotations[0], annotations[1], annotations[3]
             # Get only the primary category
             if ner != 'O':
                 ner = ner.split('-')[0]
                 ner_tags[ner] += 1

def get_ner_tags():
    ner_tags = collections.Counter()
    paths = get_tag_file_paths(corpus_root)
    for path in paths: 
        with open(path, 'rb') as file_handle:
            handle_tag_file(ner_tags, file_handle)

def get_tag_file_paths(corpus_root):
    paths = []
    for root, dirs, files in os.walk(corpus_root):
        for filename in files:
            if filename.endswith(".tags"):
                paths.append(os.path.join(root, filename))
    return paths

def pluck_leaves(tree, names_set, stopwords):
    leaves = []
    for l in tree.leaves():
        leaves.append(l[0])
    res = " ".join(leaves)
    if res == res.lower() or res.lower() in stopwords:
        return False
    res = " ".join([x for x in leaves if x.lower() != x and res.lower() not in stopwords])
    print res
    if res in names_set and names_set[res] > 3:
        names_set[res] += 1
        for name_bit in res.split():
            names_set[name_bit] += 1
        return False
    names_set[res] += 1
    for name_bit in res.split():
        names_set[name_bit] += 1
    return res        

def navigate(tree, names_set, stopwords):
    if type(tree) is nltk.Tree:
        if tree.label() == "per" or tree.label() == "geo":
            res = pluck_leaves(tree, names_set, stopwords)
            if res:
                return res
        for t in tree:
            res =  navigate(t, names_set, stopwords)
            if res:
                return res
    return False

def has_named_entity(text, names_set, sent_tokenizer, stopwords, langs, chunker):
    tokenized = sent_tokenizer.tokenize(text)
    for i in tokenized:
        words = nltk.word_tokenize(i)
        tagged = nltk.pos_tag(words)
        namedEnt = chunker.parse(tagged)
        res = False
        for n_ent in namedEnt:
            res = navigate(n_ent, names_set, stopwords)
            if res and 'german' not in langs:
                return True

    return False




class NamedEntityChunker(ChunkParserI):
    def __init__(self, train_sents, **kwargs):
        assert isinstance(train_sents, Iterable)
        self.feature_detector = features
        self.tagger = ClassifierBasedTagger(
            train=train_sents,
            feature_detector=features,
            **kwargs)
 
    def parse(self, tagged_sent):
        chunks = self.tagger.tag(tagged_sent)
 
        # Transform the result from [((w1, t1), iob1), ...] 
        # to the preferred list of triplets format [(w1, t1, iob1), ...]
        iob_triplets = [(w, t, c) for ((w, t), c) in chunks]
 
        # Transform the list of triplets to nltk.Tree format
        return conlltags2tree(iob_triplets)


def write_names_file(sentences, chunker):
    names_set = collections.defaultdict(int)
    sent_detector = nltk.data.load('tokenizers/punkt/' + 'english' + '.pickle')

    for sent in sentences:
        has_named_entity(sent, names_set, sent_detector, [], ['english'], chunker)
    f = open('names', 'w')
    names = []
    for name in names_set:
        fdist_cap = BEHEADED_FDIST[0][name.capitalize()]
        fdist_total = BEHEADED_FDIST[0][name.lower()] + BEHEADED_FDIST[0][name.capitalize()] + 0.0001 
        fdist_cap_nocap = float(fdist_cap) / fdist_total
        if fdist_cap_nocap > 0.5 and names_set[name] > 1 and '.' not in name and len(name) > 2:
            f.write("%s\t%s\t%s\n" % (name, names_set[name], fdist_cap_nocap))
            names.append(name)
    f.close()
    return names

def get_chunker(book_corpus):
    corpus_root = "gmb"
    reader = read_gmb(corpus_root)
    data = list(reader)
    training_samples = data[:int(len(data) * 0.5)]
    sentences = segmentize(open(book_corpus).read(), ['english'])
    beheaded_sentences = [" ".join(x.split()[1:]) for x in sentences if len(x.split()) > 1]
    BEHEADED_FDIST.append(nltk.FreqDist(nltk.word_tokenize("\n".join(beheaded_sentences))))

    chunker = NamedEntityChunker(training_samples)
    names = write_names_file(sentences, chunker)
    
    return chunker, names + DEFAULT_NAMES_SET

