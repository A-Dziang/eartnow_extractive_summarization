

class ShorteningRates:

    def __init__(self):
        self.LenSentencesSummary = 0
        self.LenSentencesOriginal = 0
        self.LenParagraphsSummary = 0        
        self.LenParagraphsOriginal = 0
        self.LenCharsSummary = 0
        self.LenCharsOriginal = 0

    def reset(self):
        self.LenSentencesSummary = 0
        self.LenSentencesOriginal = 0
        self.LenParagraphsSummary = 0        
        self.LenParagraphsOriginal = 0
        self.LenCharsSummary = 0
        self.LenCharsOriginal = 0

    def update(self, summaries_list, original_list):
        self.LenSentencesSummary += len(summaries_list)
        self.LenSentencesOriginal += len(original_list)
        if len(summaries_list) != len(original_list):
            self.LenParagraphsSummary += 1
        self.LenParagraphsOriginal += 1
        self.LenCharsSummary += sum([len(x) for x in summaries_list])
        self.LenCharsOriginal += sum([len(x) for x in original_list])

    def merge(self, other_instance):
        self.LenSentencesSummary += other_instance.LenSentencesSummary
        self.LenSentencesOriginal += other_instance.LenSentencesOriginal
        self.LenParagraphsSummary += other_instance.LenParagraphsSummary
        self.LenParagraphsOriginal += other_instance.LenParagraphsOriginal
        self.LenCharsSummary += other_instance.LenCharsSummary
        self.LenCharsOriginal += other_instance.LenCharsOriginal

    def get_rates(self):

        if not self.LenSentencesOriginal or not self.LenCharsOriginal:
            return 1000, 1000, 1000

        sentence_rate = float(self.LenSentencesSummary) / self.LenSentencesOriginal
        paragraph_rate = float(self.LenParagraphsSummary) / self.LenParagraphsOriginal
        char_rate = float(self.LenCharsSummary) / self.LenCharsOriginal
        return sentence_rate, paragraph_rate, char_rate

    def log_to_file(self, content_unit_name, log_file):

        if (self.LenParagraphsOriginal):
            sentence_rate, paragraph_rate, char_rate = self.get_rates()
            char_rates = "%s Chars shortening rate: %s Summaries chars: %s Original chars: %s\n" \
                             % (content_unit_name, char_rate, self.LenCharsSummary, self.LenCharsOriginal)
            sentence_rates = "%s Sentence shortening rate: %s Summaries sentences: %s Original sentences: %s\n" \
                             % (content_unit_name, sentence_rate, self.LenSentencesSummary, self.LenSentencesOriginal)
            paragraph_rates = "%s Paragraph shortening rate: %s Summarized paragraphs: %s Paragraphs left as-is: %s\n" \
                             % (content_unit_name, paragraph_rate, self.LenParagraphsSummary, self.LenParagraphsOriginal)
            log_file.write(char_rates)
            log_file.write(sentence_rates)
            log_file.write(paragraph_rates)



