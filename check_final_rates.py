﻿import sys
import BeautifulSoup
import bs4
import argparse
from extractive_summary import *
import subprocess
from epub_file_handler import *
from summarizer_utils import *


class EpubChecker:
            
    def __init__(self, epub, extract_text):
        self.FileHandler = EpubFileHandler(args)
        self.Epub = epub
        self.ExtractText = extract_text 
        self.TextLen = 0
        self.TocFile = '%s/OEBPS/toc.ncx' % self.FileHandler.TmpFolder
        self.Path = self.TocFile.replace('toc.ncx', '')
        self.TocPage = None
        self.FrontPage = None
        self.TitlePage = None
        self.PrevSrc = ""
        self.CurrSoup = None


    def create_epub(self):

        self.FileHandler.unpack_epub()                           
        toc_page, front_page, title_page = get_first_pages(self.Path)
        self.TocPage = toc_page
        self.FrontPage = front_page
        self.TitlePage = title_page

        self.TOCSoup = bs4.BeautifulSoup(open(self.Path + self.TocPage))
        self.parse_toc()

        remove_file_or_folder(self.FileHandler.TmpFolder)

            
    def process_unit(self, cont):

        content_unit_path, content_unit_marker = clear_src(cont['src'])
        self.CurrSrc = content_unit_path

        if content_unit_path != self.PrevSrc:
            self.flush_soup(content_unit_path)

        if not content_unit_marker:
            self.get_paragraphs_texts()


    def parse_toc(self):

        soup = bs4.BeautifulSoup(open(self.TocFile), 'xml')
    
        for cont in soup.findAll('content'):
            self.process_unit(cont)

        self.flush_soup(self.CurrSrc)


    def flush_soup(self, content_unit_path):

        self.CurrSoup = bs4.BeautifulSoup(open(self.Path + content_unit_path), 'html.parser')
        self.PrevSrc = content_unit_path


    def get_paragraphs_texts(self):
                                   
        paragraphs = self.CurrSoup.findAll(lambda x: is_p_and_has_text(x))
        paragraphs_texts = [x.text for x in paragraphs]
        self.TextLen += len("".join(paragraphs_texts))

        if self.ExtractText:
            print " ".join(paragraphs_texts)


def get_len(args, epub):
    args.epub = epub
    args.dst_epub = "%s.tmp" % epub

    epub_checker = EpubChecker(epub, args.extract_text)
    epub_checker.create_epub()
    return epub_checker.TextLen


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--epub1', help='path to .epub', required=True)
    parser.add_argument('--epub2', help='path to .epub', required=True)
    parser.add_argument('--extract_text', help='extract text for NER', default = False)
    args = parser.parse_args()
    len1 = get_len(args, args.epub1)
    len2 = get_len(args, args.epub2)

    print len1
    print len2
    print float(len2) / len1

