﻿# -*- coding: utf-8 -*-                                             //

import sys
reload(sys)
sys.setdefaultencoding("utf-8")
import WGGraph as wg
import os
import re
import argparse
import codecs
import itertools
from summarizer_utils import *
import sys
sys.setrecursionlimit(15000)

rankingModes={"C":"Centroid","TR":"textrank", "CW":"contentWeighing"}

QUOTEDICT = {
    u'english': [(u'“', u'”'), (u'"', u'"')],
    u'german': [(u'»', u'«')]
    }

PARAGRAPH_STOPWORDS = {
    u'english': ['But', 'Also', 'This way', 'Besides', 'After that', 'Before that', 'His', 'Her', 'There', 'This', 'Such', 'He', 'She', 'So', 'These', 'Those', 'Then'],
    u'german': ['Aber']
    }


SENTENCE_STOPWORDS = {
    u'english': ['But', 'Also', 'That', 'Those', 'His', 'Her', 'Their', 'These', 'They', 'Their', 'She', 'He', 'So'],
    u'german': ['Aber']
    }

SENTENCE_STOPWORDS_ANY = {
    u'english': ['that', 'those', 'his', 'her', 'their', 'these', 'they', 'their', 'she', 'he'],
    u'german': ['Aber']
    }

def get_stopwords(lang_stopwords):

    all_stopwords = ['!', '?', '.', ',', u'“', u'”', 'they', 'those', 'these', 'this', 'but', 'there', 'here', 'she', 'he']
    for stopwords_lang in lang_stopwords:
        all_stopwords.extend(stopwords.words(stopwords_lang))

    return all_stopwords

def get_first_nontag(summary):

    first_nontag = None
    first_nontag_index_ = first_nontag_index(summary, 0)

    if first_nontag_index_ is not None:
        first_nontag = summary[first_nontag_index_]
    
    return first_nontag

def split_tags_and_nontags(summary_params, summary):

    quotes = []

    for lang in summary_params.Lang:

        if lang in QUOTEDICT:
            quotes.extend(QUOTEDICT[lang])

    tags = []
    sentences_in_quotes_rule(summary, tags, quotes, True)

    return tags


def has_paragraph_stopwords(summary, html_tags, final_sentences_retained, first_nontag, entity_found, summary_params, actual_nontags):

    res = remove_paragraphs_rule(summary, html_tags, final_sentences_retained, first_nontag, entity_found, summary_params.Lang)

    if res and not is_quoted(" ".join(actual_nontags)):
        return True

    return False


def is_overly_short(actual_sentences, actual_nontags, entity_found):

    if (len(actual_sentences) > 2 and len(actual_nontags) == 1 and not is_quoted(" ".join(actual_nontags)) or 
        len(" ".join(actual_nontags).split()) < 10 and not is_quoted(" ".join(actual_nontags))):

        if not entity_found:
            return True

    return False

def remove_direct_speech(actual_sentences, sentences, tags, summary):

    nontags = [x for x in summary if x not in tags]

    if actual_sentences:
        res = remove_direct_speech_rule(sentences, tags, nontags)
        if res:
            return True

    return False

def apply_summary_restrictions(sentences, html_tags, summary, final_sentences_retained, entity_found, summary_params, order_dict):


    first_nontag = get_first_nontag(summary)
    actual_sentences = [x for x in sentences if not TAG_RE.match(x)]  # crutch removing extremely short summaries
    actual_nontags = [x for x in summary if not TAG_RE.match(x)]
    actual_tags = [x for x in html_tags if TAG_RE.match(x)]
    empty_summary = get_initial_order(order_dict, [x for x in html_tags if TAG_RE.match(x)])
    full_summary = get_initial_order(order_dict, sentences)

    # removing paragraph stopwords and overly short summaries

    if (has_paragraph_stopwords(summary, html_tags, final_sentences_retained, first_nontag, entity_found, summary_params, actual_nontags) or
        is_overly_short(actual_sentences, actual_nontags, entity_found)):
        return empty_summary

    tags = split_tags_and_nontags(summary_params, summary)

    # restriction forbidding removal of one sentence from a long paragraph

    if ((len(actual_sentences) - len(actual_nontags) == 1 and len(actual_sentences) == 2) or
        (len(actual_sentences) - len(actual_nontags) == 1 and len(actual_sentences) > 4)):
        return full_summary

#    if remove_direct_speech(actual_sentences, sentences, tags, summary):
#        return empty_summary

    return summary

def generateSummaries(sentences, len_multiplier, summary_params, next_paragraph_sentence, ranker = rankingModes['TR']):

    order_dict = dict([(x[1], x[0]) for x in enumerate(sentences)])

    all_stopwords = get_stopwords(summary_params.Lang)

    html_tags, not_html_tags, entity_found = get_html_tags_and_quotes(sentences, summary_params.Lang,
                                                        summary_params.NamesSet, summary_params.SentTokenizer,
                                                        summary_params.Chunker,
                                                        all_stopwords, next_paragraph_sentence, summary_params.NamedEntities)    
    

    final_sentences_retained=wg.solveILPFactBased(not_html_tags,
                                        all_stopwords, 
                                        ranker,
                                        l_max=int(len(not_html_tags) * len_multiplier),
                                        sbjthreshold=summary_params.SubjThreshold,
                                        objthreshold=summary_params.ObjThreshold)

    if not final_sentences_retained:
        final_sentences_retained = []

    summary = get_initial_order(order_dict, final_sentences_retained + html_tags)

    summary = apply_summary_restrictions(sentences, html_tags, summary, final_sentences_retained, entity_found, summary_params, order_dict)

    return summary


def get_beginning_and_end(paragraph_texts):

    MAX_PARAGRAPH_LENGTH = 200
    MARGIN_SIZE = 5

    # marking off at most MARGIN_SIZE short paragraphs in the beginning

    begin_index = 0

    for p_text in paragraph_texts:

        if begin_index == MARGIN_SIZE:
           break

        if len(p_text) < MAX_PARAGRAPH_LENGTH:
            begin_index += 1

        else:
            break

    begin_index += 1

    # marking off at most MARGIN_SIZE short paragraphs at the end

    end_index = len(paragraph_texts) - 1

    for i in range(len(paragraph_texts) - 1, len(paragraph_texts) - MARGIN_SIZE - 1, -1):

        if len(paragraph_texts[i]) < MAX_PARAGRAPH_LENGTH:
            end_index -= 1

        else:
            break

    return begin_index, end_index


def get_ordering_dicts(paragraph_texts, paragraph_conts):
    order_dict_texts = dict([(x[1], x[0]) for x in enumerate(paragraph_texts)])
    order_dict_conts = {}

    for i in range(0, len(paragraph_conts)):
        order_dict_conts[paragraph_conts[i]] = i

    return order_dict_texts, order_dict_conts


def generateParagraphSummaries(paragraph_texts, paragraph_conts, len_multiplier, summary_params, ranker = rankingModes['TR']):

    if not paragraph_texts or "".join(paragraph_texts) == "":
        return paragraph_conts

    order_dict_texts, order_dict_conts = get_ordering_dicts(paragraph_texts, paragraph_conts)
    all_stopwords = get_stopwords(summary_params.Lang)

    begin_index, end_index = get_beginning_and_end(paragraph_texts)

    shortenable_paragraphs = paragraph_texts[begin_index:end_index]
    shortenable_paragraphs = [x for x in shortenable_paragraphs if x and x.strip() and ' ' in x.strip()]

    if not shortenable_paragraphs:
        return paragraph_conts

    restricted, unrestricted = get_paragraph_restrictions(shortenable_paragraphs, summary_params.Lang,
                                                        summary_params.NamesSet, summary_params.SentTokenizer,
                                                        summary_params.Chunker,
                                                        all_stopwords, "", summary_params.NamedEntities)

    finalParagraphsRetained=wg.solveILPFactBased(shortenable_paragraphs,
                                            all_stopwords, 
                                            ranker,
                                            l_max=int(len(shortenable_paragraphs) * len_multiplier),
                                            sbjthreshold=summary_params.SubjThreshold,
                                            objthreshold=summary_params.ObjThreshold )

    finalParagraphsRetained.extend(restricted)

    if not finalParagraphsRetained:
        summary = [paragraph_conts[0]] + [paragraph_conts[-1]]
        return summary
 
    finalTextsRetained = [x[1] for x in order_dict_texts.items() if x[0] in finalParagraphsRetained]
    finalContsRetained = [x[0] for x in order_dict_conts.items() if x[1] in finalTextsRetained]
    summary = paragraph_conts[: begin_index] + get_initial_order(order_dict_conts, finalContsRetained) + paragraph_conts[end_index :]

    return summary

def clear_text(text):

    open_tag_ind = text.find('>')
    attrs = text[0 : open_tag_ind + 1]
    text_part = text[open_tag_ind + 1 : len(text)]
    closing_tag_ind = text_part.rfind('</p>')

    if closing_tag_ind != -1:
        text_part = text_part[0 : closing_tag_ind] 

    return attrs, ' '.join(text_part.replace('\n', '').split())


def get_initial_order(order_dict, result):
    result_dict = [(order_dict[x], x) for x in order_dict if x in result]
    result_order = sorted(result_dict, key=lambda x: x[0])
    return [x[1] for x in result_order]

DIGITS = re.compile(u'[0-9]-', re.U)
PUNCT_RE = re.compile('[.,!?:\-\'\"]', re.U)

def has_saved_named_entity(text, named_entities, names_set, ner_threshold, relatives_threshold):

    clean_text = PUNCT_RE.sub('', text.replace(',', ''))
    words = clean_text.replace('`s', '').split()

    if not words:
        return False

    for named_entity in named_entities:

        if ((named_entity in words or named_entity.capitalize() == words[0]) or
            named_entity.lower() != named_entity and named_entity in text):
            names_set[named_entity] += 1

            if (names_set[named_entity] < ner_threshold or
                named_entity.lower() == named_entity and names_set[named_entity] < relatives_threshold):
                return True

    return False


LINKWORDS_DICT = {
    'german' : (['So', 'Darüber', 'Vor allem', 'Und', 'Doch', 'Deswegen', 'Daher', 'Deshalb', 'Aber', 'Denn', 'Dazu', 'Dort', 'Dann', 'Hierbei'], # beginnings of next sentences
                ['dazu', 'deshalb', 'deswegen', 'daher', 'aber'], # anywhere in next sentences
                ['Als']), # in curr sentence
    'english' : (['This', 'That', 'Those', 'There', 'It', 'These', 'They', 'Then', 'But', 'And', 'Of these', 'Of this', 'Of that', 'This way', 'That\'s why', 'That\'s how', 'Therefore', 'Then', 'Suddenly', 'Later', 'Before', 'But', 'And', 'Nevertheless', 'Despite that'], # beginnings of next sentences
                ['also', 'besides', 'furthermore', 'there', 'thus', 'they', 'this', 'those', 'there', 'she', 'he', 'that', 'it'], # anywhere in next sentences
                ['—', '—', '?', ':']) # in prev sentence
    }


def fuzzy_startswith(sentence, word):

    pos = sentence.find(word)

    if  pos >= 0 and pos < 3 and word in sentence.replace(',', '').replace('!', '').replace('.', '').split():
        return True

    return False


def fuzzy_endswith(sentence, word):

    pos = sentence.rfind(word)

    if  pos >= 0 and len(sentence) - pos < 5:
        return True

    return False


def check_linkwords(sentence, beginnings, middles, ends):

    for word in middles:
        if sentence.find(word) != -1 and word in sentence.replace(',', ' ').replace('!', '').replace('.', '').split():
            return True

    for word in beginnings:
        if fuzzy_startswith(sentence, word):
            return True

    for word in ends:
        if fuzzy_endswith(sentence, word):
            return True

    return False
    
def linkwords(sentences, langs, index, next_paragraph_sent):

    if langs != ['german'] and langs != ['english']:
        return False

    lang = langs[0]
    beginnings = LINKWORDS_DICT[lang][0]
    middles = LINKWORDS_DICT[lang][1]
    ends = LINKWORDS_DICT[lang][2]         

    nontag_index = first_nontag_index(sentences, index + 1)
    if nontag_index is None and index != len(sentences) - 1:
        return False

    prev_nontag_index = r_first_nontag_index(sentences, index)

    if prev_nontag_index is not None and check_linkwords(sentences[prev_nontag_index], [], [], ends):
        return True

    if index != len(sentences) - 1 and check_linkwords(sentences[nontag_index], beginnings, middles, ends):
        return True

    else:
        if index == len(sentences) - 1 and check_linkwords(next_paragraph_sent, beginnings, middles, ends):
            return True

    return False


def quoted_open(sentence, quote_open, quote_close):
    return (quote_open in sentence and quote_close not in sentence) or sentence.count(quote_open) > sentence.count(quote_close)

def quoted_both(sentence, quote_open, quote_close):
    return quote_open in sentence and sentence.count(quote_open) == sentence.count(quote_close)

def quoted_close(sentence, quote_open, quote_close):
    return (quote_close in sentence and quote_open not in sentence) or sentence.count(quote_close) > sentence.count(quote_open)


def has_sentence_stopword_rule(stopwords, sentence, is_start):

    for stopword in stopwords:

        pos = sentence.find(stopword)

        if pos > -1 and pos < 3 and is_start:
            return True

        if pos > -1 and not is_start:
            return True

    return False

def quoted_by_quotepair(sentence, quotepair, tags, quoted, inclusive):

    if quoted_open(sentence, quotepair[0], quotepair[1]):
        quoted = True
        tags.append(sentence)

    if quoted_both(sentence, quotepair[0], quotepair[1]):
        tags.append(sentence)

    if quoted and inclusive:
        tags.append(sentence)

    if quoted_close(sentence, quotepair[0], quotepair[1]):
        tags.append(sentence)
        quoted = False

    return quoted

def sentences_in_quotes_rule(sentences, tags, quotes, inclusive):

    quoted = False

    for sentence in sentences:
        if sentence.rfind("—") != -1 and abs(len(sentence) - sentence.rfind("—")) < 4:
            tags.append(sentence)
        if sentence.rfind("?") != -1 and abs(len(sentence) - sentence.rfind("?")) < 4:
            tags.append(sentence)

        for quotepair in quotes:
            quoted = quoted_by_quotepair(sentence, quotepair, tags, quoted, inclusive)


def remove_paragraphs_rule(sentences, tags, nontags, first_nontag, entity_found, langs):

    if first_nontag and not entity_found:

        for stopword in PARAGRAPH_STOPWORDS[langs[0]]:
            pos = first_nontag.strip().find(stopword)
            
            if  pos >= 0 and pos < 5 and stopword in first_nontag.strip().split():
                nontags = []
                tags = [x for x in sentences if TAG_RE.match(x)]
                return tags, nontags

    return None


def remove_direct_speech_rule(sentences, tags, nontags):   ### TODO: clean this part up (or remove!)

    actual_quotes = [x for x in tags if not TAG_RE.match(x)]
    actual_notquotes = [x for x in sentences if not TAG_RE.match(x)]

    quotes_len = len("".join(actual_quotes))
    notquotes_len = len("".join(actual_notquotes))
    quotes_ratio =  float(quotes_len) / notquotes_len

    if quotes_ratio > 0.6 and len(actual_quotes) > 2:
        nontags = []
        tags = [x for x in sentences if TAG_RE.match(x)]
        return tags, nontags

    return None
   
    

def tag_or_not_tag(sentences, index, tags, nontags, names_set, sent_tokenizer, stopwords, langs, chunker, next_sent, named_entities):

    entity_found = False

    if has_saved_named_entity(sentences[index], named_entities, names_set, 5, 15): 
        tags.append(sentences[index])
        entity_found = True

    colon_pos = sentences[index].rfind(':')

    if (TAG_RE.match(sentences[index]) or #or # or unclosed_quotes(sentences[index], quotes) or
                    linkwords(sentences, langs, index, next_sent)# or '-' in sentences[index]):
                    or colon_pos >= 0 and colon_pos < len(sentences[index]) - 4):
        tags.append(sentences[index])

    else:

        if sentences[index] not in tags:

            if (not has_sentence_stopword_rule(SENTENCE_STOPWORDS[langs[0]], sentences[index], True)):# and 
                nontags.append(sentences[index])

    return entity_found


def restricted_paragraph(sentences, index, tags, nontags, names_set, sent_tokenizer, stopwords, langs, chunker, next_sent, named_entities):
#    if has_saved_named_entity(sentences[index], named_entities, names_set, 3, 5): 
    if has_saved_named_entity(sentences[index], named_entities, names_set, 2, 3): 
        tags.append(sentences[index])

    if (index > 1 and has_dash_or_question(sentences[index - 1]) and is_quoted(sentences[index - 1])):
#        or has_dash_or_question(sentences[index]) and is_quoted(sentences[index])):
        tags.append(sentences[index])


def get_html_tags_and_quotes(sentences, langs, names_set, sent_tokenizer, chunker, stopwords, next_sent, named_entities):

    quotes = []
    for lang in langs:
        if lang in QUOTEDICT:
            quotes.extend(QUOTEDICT[lang])

    tags = []
    nontags = []
    sentences_in_quotes_rule(sentences, tags, quotes, False)

    entity_found = False
    for index in range(0, len(sentences)):
        entity_found_here = tag_or_not_tag(sentences, index, tags, nontags, names_set, sent_tokenizer, stopwords, langs, chunker, next_sent, named_entities)
        entity_found = entity_found or entity_found_here 

    return tags, nontags, entity_found


def get_paragraph_restrictions(sentences, langs, names_set, sent_tokenizer, chunker, stopwords, next_sent, named_entities):

    quotes = []
    for lang in langs:
        if lang in QUOTEDICT:
            quotes.append(QUOTEDICT[lang])

    tags = []
    nontags = []
#    sentences_in_quotes_rule(sentences, tags, quotes, False)
                   
    for index in range(0, len(sentences)):
        entity_found_here = restricted_paragraph(sentences, index, tags, nontags, names_set, sent_tokenizer, stopwords, langs, chunker, next_sent, named_entities)

    return tags, nontags

  