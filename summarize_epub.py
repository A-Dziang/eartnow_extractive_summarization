﻿import sys
import BeautifulSoup
import bs4
import argparse
from extractive_summary import *
import subprocess
import itertools
import re
import copy


#----------logging and editorial notes-------------

EDITORIAL_NOTE = {
    'english':(["This eBook follows the original text.", "This eBook is an exact transcription of the original text."],
               "This eBook has been carefully abridged to %.0f %s of the original unabridged edition published by e-artnow:"),
    'german':(["Dieses eBuch folgt dem Originaltext."],
              "Quickie Klassiker sind sorgfältig gekürzte Versionen der Literatur-Klassiker. Die leserfreundlichen E-Books bewahren \
               gänzlich Sprache und Stil des Autors der Originalfassung, um der \"Essenz\" des Originals treu zu bleiben. Die Quickie \
               Klassiker sparen Ihnen nicht nur Zeit, sondern erhalten gleichzeitig noch die Authentizität und die Herrlichkeit dieser \
               literarischen Meisterstücke. Entdecken Sie wahre Lesefreude neu, indem Sie in die zeitlosen Geschichten der kultigen Autoren \
               eintauchen! Dieses E-Book wurde sorgfältig und angemessen auf %.0f %s der ungekürzten Originalversion von e-artnow gekürzt:")
    }



class ShorteningRates:

    def __init__(self):
        self.LenSentencesSummary = 0
        self.LenSentencesOriginal = 0
        self.LenParagraphsSummary = 0        
        self.LenParagraphsOriginal = 0
        self.LenCharsSummary = 0
        self.LenCharsOriginal = 0

    def reset(self):
        self.LenSentencesSummary = 0
        self.LenSentencesOriginal = 0
        self.LenParagraphsSummary = 0        
        self.LenParagraphsOriginal = 0
        self.LenCharsSummary = 0
        self.LenCharsOriginal = 0

    def update(self, summaries_list, original_list):
        self.LenSentencesSummary += len(summaries_list)
        self.LenSentencesOriginal += len(original_list)
        if len(summaries_list) != len(original_list):
            self.LenParagraphsSummary += 1
        self.LenParagraphsOriginal += 1
        self.LenCharsSummary += sum([len(x) for x in summaries_list])
        self.LenCharsOriginal += sum([len(x) for x in original_list])

    def merge(self, other_instance):
        self.LenSentencesSummary += other_instance.LenSentencesSummary
        self.LenSentencesOriginal += other_instance.LenSentencesOriginal
        self.LenParagraphsSummary += other_instance.LenParagraphsSummary
        self.LenParagraphsOriginal += other_instance.LenParagraphsOriginal
        self.LenCharsSummary += other_instance.LenCharsSummary
        self.LenCharsOriginal += other_instance.LenCharsOriginal

    def get_rates(self):
        if not self.LenSentencesOriginal:
            return 1000, 1000, 1000
        sentence_rate = float(self.LenSentencesSummary) / self.LenSentencesOriginal
        paragraph_rate = float(self.LenParagraphsSummary) / self.LenParagraphsOriginal
        char_rate = float(self.LenCharsSummary) / self.LenCharsOriginal
        return sentence_rate, paragraph_rate, char_rate

    def log_to_file(self, content_unit_name, log_file):
        if (self.LenParagraphsOriginal):
            sentence_rate, paragraph_rate, char_rate = self.get_rates()
            char_rates = "%s Chars shortening rate: %s Summaries chars: %s Original chars: %s\n" \
                             % (content_unit_name, char_rate, self.LenCharsSummary, self.LenCharsOriginal)
            sentence_rates = "%s Sentence shortening rate: %s Summaries sentences: %s Original sentences: %s\n" \
                             % (content_unit_name, sentence_rate, self.LenSentencesSummary, self.LenSentencesOriginal)
            paragraph_rates = "%s Paragraph shortening rate: %s Summarized paragraphs: %s Paragraphs left as-is: %s\n" \
                             % (content_unit_name, paragraph_rate, self.LenParagraphsSummary, self.LenParagraphsOriginal)
            log_file.write(char_rates)
            log_file.write(sentence_rates)
            log_file.write(paragraph_rates)


#--------------------------------------------------

SIGIL_TOC_ID_RE = re.compile('[A-z_-]+\d+')

# clearing away sigil markers 
def clear_src(src):
    if '#' in src:
        if not SIGIL_TOC_ID_RE.match(src.split('#')[1]):
            print "ALARM!!! BAD ID!!!"
#            exit()
        return src.split('#')[0], src.split('#')[1] 
    return src, ""


def replace_summarized_tag(paragraph, attrs, summary, credible_text_len):
    summary_text = " ".join(summary)
    extra_soup = bs4.BeautifulSoup(attrs + summary_text + "</p>")
    if len(extra_soup.p.text) > credible_text_len:
        paragraph.replace_with(extra_soup.p)


def get_split_index(sentences, searchReverse): # return first or last sentence which is not an html tag
    if searchReverse:
        for index in xrange(len(sentences) - 1, -1, -1):
            if not TAG_RE.match(sentences[index]):
                return index
        return len(sentences)
    else: 
        for index in xrange(0, len(sentences) - 1):
            if not TAG_RE.match(sentences[index]):
                return index
        return 0

def is_p_and_has_text(tag, credible_text_len):
    return tag.name == 'p' and tag.string and len(tag.string) > credible_text_len
         

def has_no_sigil_marker(element):
    return ('id' not in element.attrs or not element['id'] or
            not (element['id'].startswith('sigil') and not element['id'].startswith('e-art_id')))
    

def descend_tree(the_tag, editorial_note):
    for x in the_tag.children:
        if not isinstance(x, bs4.NavigableString) and has_editorial_note(editorial_note, x.text):
            return descend_tree(x, text)
    return the_tag


def has_editorial_note(editorial_note, text):
    for note_version in editorial_note:
        if note_version in text:
            return True
    return False


def process_lang(front_soup, editorial_note, new_editorial_note):

    editorial_span = front_soup.find(lambda x: x.name == 'span' and                                            
                                     has_editorial_note(editorial_note, x.text))
    if not editorial_span:
        editorial_span = front_soup.find(lambda x: x.name == 'div' and
                                         has_editorial_note(editorial_note, x.text))

    saved_editorial_span = editorial_span
    editorial_span = descend_tree(editorial_span, editorial_note)

    try:
        editorial_span.string.replace_with(new_editorial_note)
    except:
        for content in editorial_span.contents:
            if editorial_note in content:
               content.replace_with(new_editorial_note)

    return saved_editorial_span

def get_title(front_soup):
    title = front_soup.new_tag('div')
    title.string = front_soup.find('h1').text
    return title

def get_isbn(front_soup): 
    isbn_div = front_soup.find(lambda x: x.name == 'span' and 'ISBN' in x.text)
    if not isbn_div:
        isbn_div = front_soup.find(lambda x: x.name == 'div' and 'ISBN' in x.text)
    isbn = front_soup.new_tag('div')
    isbn.string = isbn_div.string 
    return isbn

def get_image(title_soup):
    image_div = title_soup.new_tag('div') 
    image = title_soup.find('svg').extract()
    image['xmlns'] = "http://www.w3.org/2000/svg"
    image['xmlns:xlink'] = "http://www.w3.org/1999/xlink"
    image.image['xlink:href'] = image.image['href']
    del image.image.attrs['href']
    image_div.append(image)
    return image_div

def filter_tags(src):
    return [x for x in src if TAG_RE.match(x) == None]

def process_first_pages(path, first_page, title_page, langs, final_rate):

    front_soup = bs4.BeautifulSoup(open(path + first_page), 'lxml')
    title_soup = bs4.BeautifulSoup(open(path + title_page), 'xml')
    saved_editorial_span = None

    for lang in langs:
        saved_editorial_span = process_lang(front_soup, EDITORIAL_NOTE[lang][0], EDITORIAL_NOTE[lang][1] % (final_rate * 100, "%"))


    while len(saved_editorial_span.find_all('b')) < len(langs): 
        saved_editorial_span = saved_editorial_span.parent

    new_elements = [
                    get_title(front_soup),
                    get_isbn(front_soup),
                    get_image(title_soup)
                   ]

    for new_element in new_elements:
        saved_editorial_span.append(new_element)

    open(path + first_page, 'w').write(front_soup.prettify())    

class EpubSummarizer:
            
    def __init__(self, args):
        self.Epub = args.epub
        if ',' in args.lang:
            self.Lang = args.lang.split(',')
        else:
            self.Lang = [args.lang]
        self.CredibleTextLength = args.min_text_len              # min text length in paragraphs
        self.MaxUnsummarizableSentences = args.min_sentences     # min sentences in a paragraph which can be shortened
        self.MaxUnsummarizableSymbols = args.min_symbols         # min symbols in a paragraph which can be shortened
        self.DstEpub = args.dst_epub
        self.ShortRateIterations = args.short_rate_iterations
        self.SubjThreshold = args.subj_threshold
        self.ObjThreshold = args.obj_threshold
        self.SrcZip = self.Epub.replace('.epub', '.zip')
        self.DstZip = self.DstEpub.replace('.epub', '.zip')
        self.TmpFolder = 'tmp'
        self.MakeTmpFolder = 'mkdir %s;' % self.TmpFolder
        self.UnzipPartial = 'mkdir %s; cd %s; cp ../%s .; unzip %s;' % (self.TmpFolder, self.TmpFolder, self.SrcZip, self.SrcZip)
        self.UpdateZip = 'cp %s %s; cp %s ../' % (self.DstZip, self.SrcZip, self.SrcZip)
        self.Unzip = 'cp %s ./%s/%s; cd %s; unzip %s;' % (self.Epub, self.TmpFolder, self.SrcZip, self.TmpFolder, self.SrcZip)
        self.RemoveOld = 'rm -f ../%s' % (self.DstEpub)
        self.RemoveTmpFiles = 'cp %s %s; rm %s; rm -rf %s' % (self.DstZip, self.DstEpub, self.DstZip, self.TmpFolder)
        self.RemoveTmp = 'rm -rf %s' % (self.TmpFolder)
        self.LogFile = open(self.DstEpub.replace('.epub', '.log'), 'w')
        self.CdTmp = 'cd %s;' % self.TmpFolder
        self.ZipMimetype = 'zip -X ../%s mimetype;' % self.DstZip
        self.ZipMetainf = 'zip -rX ../%s META-INF;' % self.DstZip
        self.ZipOebps = 'zip -rX ../%s OEBPS;' % self.DstZip
        self.TotalRates = ShorteningRates()
        self.NovelRates = ShorteningRates()
        self.ChapterRates = ShorteningRates()
        self.SigilChapterRates = ShorteningRates()
        self.PrevTOCRef = None
        self.TOCRef = None
        self.TocFile = '%s/OEBPS/toc.ncx' % self.TmpFolder
        self.Path = self.TocFile.replace('toc.ncx', '')
        self.PrevSrc = ''
        self.CurrSoup = None
        self.TOCRef = None
        self.PrevTOCRef = None
        self.TOCModeOn = False
        self.NotFirst = False
        self.Content = {}

    def convert_html_to_xhtml(self):
        files = subprocess.check_output(["find %s -type f" % self.TmpFolder], shell=True).split('\n')
        print files
        tmp_file = 'tmp_file'
        text_files = [x for x in files if '.epub' not in x and '.zip' not in x and x]
        for file in text_files:
            subprocess.check_output(["cat %s | sed -e 's/\.html/.xhtml/g' > %s" % (file, tmp_file)], shell=True)
            subprocess.check_output(["cp %s %s" % (tmp_file, file.replace('.html', '.xhtml'))], shell=True)
            if '.html' in file:
                subprocess.check_output(["rm %s" % file], shell=True)
            
        

    def parse_content(self):
        self.TocPage = None
        self.FrontPage = None
        self.TitlePage = None

        content_soup = bs4.BeautifulSoup(open(self.Path + 'content.opf'))
        important_pages = content_soup.spine.find_all('itemref')
        for page in important_pages:
            idref = page['idref']
            if 'toc.' in idref or 'content.' in idref or 'contents.' in idref or 'TOC.' in idref:
                self.TocPage = 'Text/' + idref
                if 'xhtml' not in idref:
                   self.TocPage += '.xhtml'
            if 'front' in idref:
                self.FrontPage = 'Text/' + idref
                if 'xhtml' not in idref:
                   self.FrontPage += '.xhtml'
            if 'title' in idref or 'cover' in idref:
                self.TitlePage = 'Text/' + idref
                if 'xhtml' not in idref:
                   self.TitlePage += '.xhtml'

        print self.TocPage
        content = content_soup.manifest.find_all('item')
        for item in content:
            self.Content[item['id']] = item['href']

    def create_epub(self):

        unzip_to_folder_initial = " ".join([self.MakeTmpFolder, self.Unzip, self.RemoveOld])
        unzip_to_folder = " ".join([self.UnzipPartial, self.RemoveOld])
        print subprocess.check_output(unzip_to_folder_initial, shell=True)
        self.convert_html_to_xhtml()                           
        self.parse_content()
        print self.TocPage
        self.TOCSoup = bs4.BeautifulSoup(open(self.Path + self.TocPage))
        print subprocess.check_output(" ".join([self.CdTmp, self.ZipMimetype, self.ZipMetainf, self.ZipOebps]), shell=True)
        print subprocess.check_output(self.UpdateZip, shell=True)
        print subprocess.check_output(self.RemoveTmp, shell=True)
        print subprocess.check_output(unzip_to_folder, shell=True)

        if self.TOCSoup:
            self.TOCModeOn = True
        total_rates = self.parse_toc()

        print subprocess.check_output(" ".join([self.CdTmp, self.ZipMimetype, self.ZipMetainf, self.ZipOebps]), shell=True)
        print subprocess.check_output(self.RemoveTmpFiles, shell=True)

        self.LogFile.write("\n\n")
        total_rates.log_to_file("Total", self.LogFile)
        self.LogFile.close()


    def process_novel_end(self, src):
        print src
        self.TOCRef = self.TOCSoup.find(lambda x: x.name == 'a' and x.has_attr('href') and src in x['href'])
        if not self.TOCRef: 
            self.NovelRates.merge(self.ChapterRates)
            self.NotFirst = True
        else:
            if self.PrevTOCRef:
                self.append_rate_mark()
            self.PrevTOCRef = self.TOCRef
            self.NovelRates.reset()
            self.NovelRates.merge(self.ChapterRates)
            self.NotFirst = False
            
            
    def process_unit(self, cont):
        src = cont['src']
        print src
        content_unit_path, content_unit_marker = clear_src(src)
        self.CurrSrc = content_unit_path
#        real_content_unit_path = self.Content[content_unit_path]

        if content_unit_path != self.PrevSrc:
            self.flush_soup(content_unit_path)
            self.SigilChapterRates.reset()

        if content_unit_marker:
            self.text_from_sigil_marker(content_unit_marker)
        else:
            self.text_from_content_unit()

        self.TotalRates.merge(self.ChapterRates)
        if self.TOCModeOn:
            self.process_novel_end(src)
#            self.NovelRates.log_to_file(curr_prefix, self.LogFile)
        self.SigilChapterRates.merge(self.ChapterRates)
        self.ChapterRates.log_to_file(content_unit_path + content_unit_marker, self.LogFile)
        self.ChapterRates.reset()


    def parse_toc(self):
        soup = bs4.BeautifulSoup(open(self.TocFile), 'xml')
    
        for cont in soup.findAll('content'):
            self.process_unit(cont)

        self.append_rate_mark()

        if self.TOCModeOn:
            open(self.Path + self.TocPage, 'w').write(self.TOCSoup.prettify())
        _, _, char_rate = self.TotalRates.get_rates()

        process_first_pages(self.Path, self.FrontPage, self.TitlePage, self.Lang, char_rate)
                                                     
        return self.TotalRates


    def append_rate_mark(self):
        if self.NovelRates.LenParagraphsOriginal and self.TOCModeOn:
            _, _, char_rate = self.NovelRates.get_rates()
            rate_mark = self.TOCSoup.new_tag('span')
            rate_mark.string = " (%.0f %s)" % (char_rate * 100, "%")
            self.PrevTOCRef.append(rate_mark)
            print char_rate
            print "APPENDED NOVEL MARK"
            print self.PrevTOCRef.text

    def flush_soup(self, content_unit_path):
        if self.CurrSoup:
            subprocess.call('rm %s' % self.Path + self.PrevSrc, shell=True)  
            open(self.Path + self.PrevSrc, 'w').write(self.CurrSoup.prettify())
        self.CurrSoup = bs4.BeautifulSoup(open(self.Path + content_unit_path), 'xml')
        self.PrevSrc = content_unit_path

    def text_from_sigil_marker(self, content_unit_marker):

        elements = self.CurrSoup.findAll(id=content_unit_marker)
        paragraphs = []
        if elements:
            element = elements[0].find_next_sibling('p')
            while element and has_no_sigil_marker(element):
                paragraphs.append(element)
                element = element.find_next_sibling('p')
            self.get_summary(paragraphs)


    def text_from_content_unit(self):
        paragraphs = self.CurrSoup.findAll(lambda x: is_p_and_has_text(x, self.CredibleTextLength))
        self.get_summary(paragraphs)


    def fix_start_sentence(self, list_sentences, shortening_rate):
        split_index = get_split_index(list_sentences, False)
        first_sentence = list_sentences[0 : split_index + 1]
        summarizable_rest = list_sentences[split_index + 1 : len(list_sentences)]
        return first_sentence + generateSummaries(summarizable_rest,
                                                  len_multiplier=shortening_rate,
                                                  stopwords_langs=self.Lang,
                                                  sbjthreshold=self.SubjThreshold,
                                                  objthreshold=self.ObjThreshold)


    def fix_end_sentence(self, list_sentences, shortening_rate):
        split_index = get_split_index(list_sentences, True)
        last_sentence = list_sentences[split_index : len(list_sentences)]
        summarizable_rest = list_sentences[0 : split_index]
        return generateSummaries(summarizable_rest,
                                 len_multiplier=shortening_rate,
                                 stopwords_langs=self.Lang,
                                 sbjthreshold=self.SubjThreshold,
                                 objthreshold=self.ObjThreshold) + last_sentence

    def make_single_summary(self, paragraphs, p_index, shortening_rate, list_sentences, attrs):

        if p_index == 0:
            summary = self.fix_start_sentence(list_sentences, shortening_rate)
        elif p_index == len(paragraphs) - 1:
            summary = self.fix_end_sentence(list_sentences, shortening_rate)
        else:
            summary = generateSummaries(list_sentences,
                                        len_multiplier=shortening_rate,
                                        stopwords_langs=self.Lang,
                                        sbjthreshold=self.SubjThreshold,
                                        objthreshold=self.ObjThreshold)

        empty_start_or_end_summary = (len(summary) == 1 and
                                      (p_index == 0 or p_index == len(paragraphs) - 1))
        
        if len(summary) and not empty_start_or_end_summary and len("".join(summary)) > self.CredibleTextLength:
            self.ChapterRates.update(filter_tags(summary), filter_tags(list_sentences))
            replace_summarized_tag(paragraphs[p_index], attrs, summary, self.CredibleTextLength)
        else:
            self.ChapterRates.update(filter_tags(list_sentences), filter_tags(list_sentences))


    def summarize_paragraph(self, paragraphs, p_index, shortening_rate):
        attrs, cleared_text = clear_text(paragraphs[p_index].prettify())
        list_sentences = segmentize(cleared_text, self.Lang)
        if (len(list_sentences) <= self.MaxUnsummarizableSentences and 
            len(cleared_text) <= self.MaxUnsummarizableSymbols):
            self.ChapterRates.update(filter_tags(list_sentences), filter_tags(list_sentences))
            return

        if (len(list_sentences) <= self.MaxUnsummarizableSentences + 5 and 
            len(cleared_text) <= self.MaxUnsummarizableSymbols + 100) and not self.NotFirst:
            self.ChapterRates.update(filter_tags(list_sentences), filter_tags(list_sentences))
            return

        try:
            self.make_single_summary(paragraphs, p_index, shortening_rate, list_sentences, attrs)
        except ValueError:
            pass


                  
    def get_summary(self, paragraphs):
        min_rate = 10000
        min_fin_rate = 0
        rate = 20
        rate_list = []


        for i in range(0, self.ShortRateIterations):
            for p_index in range(0, len(paragraphs)):
                self.summarize_paragraph(paragraphs, p_index, rate)
            _, _, char_rate = self.ChapterRates.get_rates()
            self.ChapterRates.reset()
            rate_list.append(char_rate)
            if char_rate < min_rate:
                min_rate = char_rate
                min_fin_rate = rate
                rate = 0.6 * rate
            else:
                rate = 1.2 * rate

        for p_index in range(0, len(paragraphs)):
            self.summarize_paragraph(paragraphs, p_index, min_fin_rate)

        self.append_chapter_tag(paragraphs)
             

    def append_chapter_tag(self, paragraphs):
        if self.CurrSrc == self.FrontPage:
            return
        if self.ChapterRates.LenParagraphsOriginal and paragraphs:
            _, _, char_rate = self.ChapterRates.get_rates()
            rate_mark = self.CurrSoup.new_tag('i')
            rate_mark.string = " (%.0f %s)" % (char_rate * 100, "%")
            prev_element = paragraphs[-1].find_previous(['h3', 'h2', 'h1'])
            if not prev_element:
                prev_element = self.CurrSoup.find(['h3', 'h2', 'h1'])


            if prev_element:
                old_mark = prev_element.find(lambda x: x.name == 'i' and '%' in x.text)
                if not old_mark:
                    prev_element.append(rate_mark)
                else:
                    _, _, char_rate = self.SigilChapterRates.get_rates()
                    rate_mark.string = " (%.0f %s)" % (char_rate * 100, "%")
                    old_mark.replace_with(rate_mark) # add merging here





if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--epub', help='path to .epub', required=True)
    parser.add_argument('--lang', help='stopwords lang', required=True)
    parser.add_argument('--dst_epub', help='dst epub', required=True)
    parser.add_argument('--short_rate_iterations', type=int, help='shortening rate - a float', required=True)
    parser.add_argument('--subj_threshold', type=float, help='subj_threshold', default=0.3)
    parser.add_argument('--obj_threshold', type=float, help='obj_threshold', default=0.4)
    parser.add_argument('--min_sentences', type=int, help='minimum sentences in a paragraph to summarize', default=2)
    parser.add_argument('--min_symbols', type=int, help='minumum symbols to summarize a paragraph', default=150)
    parser.add_argument('--min_text_len', type=int, help='minimum text length in a sentence to get summarized', default=6)
    args = parser.parse_args()
    epub_summarizer = EpubSummarizer(args)
    epub_summarizer.create_epub()

