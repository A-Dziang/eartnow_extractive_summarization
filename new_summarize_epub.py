﻿import sys
import BeautifulSoup
import bs4
import argparse
from extractive_summary import *
import subprocess
import itertools
import re
import copy
import collections
import string
from nltk.stem.snowball import SnowballStemmer
from short_rates_logger import ShorteningRates
from summarizer_utils import *
from epub_file_handler import EpubFileHandler

class EpubSummarizer:
            
    def __init__(self, args):
        self.FileHandler = EpubFileHandler(args)
        self.SummaryParams = SummaryParams(args)
        self.CredibleTextLength = args.min_text_len              # min text length in paragraphs
        self.DesiredRate = args.desired_rate
        self.DesiredParagraphRate = args.desired_paragraph_rate
        self.ShortRateIterations = args.short_rate_iterations
        self.LogFile = open(self.FileHandler.DstEpub.replace('.epub', '.log'), 'w')
        self.NovelRates = ShorteningRates()
        self.ChapterRates = ShorteningRates()
        self.SigilChapterRates = ShorteningRates()
        self.PrevTOCRef = None
        self.TOCRef = None
        self.TocFile = '%s/OEBPS/toc.ncx' % self.FileHandler.TmpFolder
        self.Path = self.TocFile.replace('toc.ncx', '')
        self.PrevSrc = ''
        self.CurrSrc = None
        self.CurrSoup = None
        self.TOCRef = None
        self.PrevTOCRef = None
        self.TOCModeOn = False
        self.NotFirst = False
        self.OrigChapterLen = {}
        self.OrigNovelLen = collections.defaultdict(int)
        self.SummaryNovelLen = collections.defaultdict(int)
        self.OrigLen = 0
        self.SummaryLen = 0
      

    def create_epub(self):

        self.FileHandler.convert_and_unpack_epub()
        self.SummaryParams.Chunker = self.FileHandler.Chunker
        self.SummaryParams.NamedEntities = self.FileHandler.NamedEntities

        toc_page, front_page, title_page = get_first_pages(self.Path)
        self.TocPage = toc_page
        self.FrontPage = front_page
        self.TitlePage = title_page
        self.TOCSoup = bs4.BeautifulSoup(open(self.Path + self.TocPage))

        if self.TOCSoup:
            self.TOCModeOn = True

        self.parse_toc(False)
        self.SummaryParams.NamesSet = collections.defaultdict(int)
        self.parse_toc(True)

        char_rate = min(float(self.SummaryLen) / self.OrigLen, 1)

        process_first_pages(self.Path, self.FrontPage, self.TitlePage, self.SummaryParams.Lang, char_rate)

        self.FileHandler.pack_epub()



    def process_novel_end(self, src, paragraph_mode):

        self.TOCRef = self.TOCSoup.find(lambda x: x.name == 'a' and x.has_attr('href') and src in x['href'])

        if not self.TOCRef:
            self.NovelRates.merge(self.ChapterRates)
            self.NotFirst = True

        else:

            if self.PrevTOCRef and paragraph_mode == True:
                self.append_rate_mark()

            if paragraph_mode == False:

               if self.PrevTOCRef:
                   self.OrigNovelLen[self.TOCRef['href']] += self.ChapterRates.LenCharsOriginal

            else:   

               if self.PrevTOCRef:
                   self.SummaryNovelLen[self.TOCRef['href']] += self.ChapterRates.LenCharsSummary

            self.PrevTOCRef = self.TOCRef
            self.NovelRates.reset()
            self.NovelRates.merge(self.ChapterRates)
            self.NotFirst = False


    def handle_soup_change(self, content_unit_path): 

        self.CurrSrc = content_unit_path

        if content_unit_path != self.PrevSrc:
            self.flush_soup(content_unit_path)
            self.SigilChapterRates.reset()

        else:
            subprocess.call('rm %s' % self.Path + self.CurrSrc, shell=True)  
            open(self.Path + self.CurrSrc, 'w').write(str(self.CurrSoup))
            self.CurrSoup = bs4.BeautifulSoup(open(self.Path + content_unit_path), 'html.parser')
                       

    def process_unit(self, cont, paragraph):

        src = cont['src']
        content_unit_path, content_unit_marker = clear_src(src)
        self.handle_soup_change(content_unit_path)            

        if not content_unit_marker:
            content_unit_marker = ""

        if paragraph:
            self.paragraph_from_sigil_marker(content_unit_marker)

        else:
            self.text_from_sigil_marker(content_unit_marker)


        if self.TOCModeOn:
            self.process_novel_end(src, False)

        self.SigilChapterRates.merge(self.ChapterRates)
        self.ChapterRates.log_to_file(content_unit_path + content_unit_marker, self.LogFile)
        self.ChapterRates.reset()


    def parse_toc(self, paragraph):

        soup = bs4.BeautifulSoup(open(self.TocFile), 'xml')

        for cont in soup.findAll('content'):
            self.process_unit(cont, paragraph)

        self.flush_soup(self.CurrSrc)

        if paragraph:
              
            self.append_rate_mark()
    
            if self.TOCModeOn:
                open(self.Path + self.TocPage, 'w').write(self.TOCSoup.prettify())

                                              
    def append_rate_mark(self):

        if self.NovelRates.LenParagraphsOriginal and self.TOCModeOn and self.OrigNovelLen[self.PrevTOCRef['href']] and self.SummaryNovelLen[self.PrevTOCRef['href']]:
            char_rate = min(float(self.SummaryNovelLen[self.PrevTOCRef['href']]) / self.OrigNovelLen[self.PrevTOCRef['href']], 1) 
            rate_mark = self.TOCSoup.new_tag('span')
            rate_mark.string = " (%.0f %s)" % (char_rate * 100, "%")
            self.PrevTOCRef.append(rate_mark)


    def flush_soup(self, content_unit_path):

        if self.CurrSoup:
            subprocess.call('rm %s' % self.Path + self.PrevSrc, shell=True)  
            open(self.Path + self.PrevSrc, 'w').write(str(self.CurrSoup))
            self.CurrLog.close()

        self.CurrSoup = bs4.BeautifulSoup(open(self.Path + content_unit_path), 'html.parser')
        self.CurrLog = open(self.Path + content_unit_path + '.summ_log', 'w')
        self.PrevSrc = content_unit_path


    def text_from_sigil_marker(self, content_unit_marker):

        if not content_unit_marker:
            elements = [self.CurrSoup.find('p')]

        else:    
            elements = self.CurrSoup.findAll(id=content_unit_marker)

        paragraphs = []

        if elements and elements[0]:
            element = elements[0].find_next()

            while element and has_no_sigil_marker(element):# and element.name != 'h3':
                id = ""

                if 'id' in element.attrs:
                    id = element['id']

                if element.name == 'p':
                    paragraphs.append(element)

                element = element.find_next()

            self.get_summary(paragraphs)


    def paragraph_from_sigil_marker(self, content_unit_marker):

        if content_unit_marker: 
            elements = self.CurrSoup.findAll(id=content_unit_marker)

        else:
            elements = [self.CurrSoup.find('p')]

        paragraphs = []

        if elements and elements[0]:
            element = elements[0].find_next()

            while element and has_no_sigil_marker(element):# and element.name != 'h3':

                id = ""

                if 'id' in element.attrs:
                    id = element['id']

                if element.name == 'p':
                    paragraphs.append(element)

                element = element.find_next()

            self.get_paragraph_summary(paragraphs)


    def make_single_summary(self, paragraphs_texts, paragraphs_conts, p_index, shortening_rate, list_sentences, attrs, next_first_sentence):

        MIN_KEEPING_LEN = 200 # differentiating short things like farewells in a letter from real endings
        BORDER = 5 # how many paragraphs to look for a long ending/beginning

        try:

            if p_index < BORDER and not [x for x in paragraphs_texts[0 : p_index] if len(x) > MIN_KEEPING_LEN]:
                summary = list_sentences                                                       

            elif (p_index == len(paragraphs_texts) - 1 or
                  p_index > len(paragraphs_texts) - BORDER and not [x for x in paragraphs_texts[p_index + 1 : ] if len(x) > MIN_KEEPING_LEN]):
                summary = list_sentences

            else:
                summary = generateSummaries(list_sentences,
                                            len_multiplier=shortening_rate,
                                            summary_params = self.SummaryParams,
                                            next_paragraph_sentence=next_first_sentence)

            if replace_summarized_tag(paragraphs_texts[p_index], attrs, summary, self.CurrSoup, self.CurrLog) == True:
                self.ChapterRates.update(filter_tags(summary), [paragraphs_texts[p_index]])

            else:
                self.ChapterRates.update([paragraphs_texts[p_index]], [paragraphs_texts[p_index]])

        except ValueError as e:
                print e
                self.ChapterRates.update([paragraphs_texts[p_index]], [paragraphs_texts[p_index]])


    def unsummarizable_paragraph_rule(self, paragraph_text, paragraph_conts):

        SHORT_TEXT_LEN = 150

        if len(paragraph_text) < self.CredibleTextLength:
            return True

        if len(paragraph_text) < SHORT_TEXT_LEN:         
            if '</i>' in paragraph_conts or '</b>' in paragraph_conts or '</small>' in paragraph_conts:
                return True

        if SPAN_RE.search(paragraph_conts) or looks_like_list_item(paragraph_text): 
            return True

        return False


    def summarize_paragraph(self, paragraphs_texts, paragraphs_conts, p_index, shortening_rate):

        attrs, cleared_text = clear_text(paragraphs_conts[p_index])
        list_sentences = segmentize(cleared_text, self.SummaryParams.Lang)
        next_sentence = ""

        if p_index < len(paragraphs_texts) - 1:
            next_list_sentences = segmentize(paragraphs_texts[p_index + 1], self.SummaryParams.Lang)

            if next_list_sentences:
                nontag_index = -1

                for i in range(0, len(next_list_sentences)):

                    if not TAG_RE.match(next_list_sentences[i]):
                        nontag_index = i

                if nontag_index != -1:
                    next_sentence = next_list_sentences[nontag_index]


        if cleared_text and self.unsummarizable_paragraph_rule(paragraphs_texts[p_index], paragraphs_conts[p_index]):
            self.ChapterRates.update([paragraphs_texts[p_index]], [paragraphs_texts[p_index]])
            return

        self.make_single_summary(paragraphs_texts, paragraphs_conts, p_index, shortening_rate, list_sentences, attrs, next_sentence)


    def reset_soup_state(self):

        self.ChapterRates.reset()
        self.CurrSoup = bs4.BeautifulSoup(open(self.Path + self.CurrSrc), 'xml')


    def summarize_chapter(self, paragraphs_texts, paragraphs_conts, rate):

        for p_index in range(0, len(paragraphs_texts)):
            self.summarize_paragraph(paragraphs_texts, paragraphs_conts, p_index, rate)


    def tune_rate(self, char_rate, rate_dict):

        DECREASE_COEFF = 0.6
        INCREASE_COEFF = 1.2

        if abs(char_rate - self.DesiredRate) < abs(rate_dict["best_rate"] - self.DesiredRate):
            rate_dict["best_rate"] = char_rate
            rate_dict["fin_rate"] = rate_dict["rate"]

        if char_rate - self.DesiredRate > 0:
            rate_dict["rate"] = DECREASE_COEFF * rate_dict["rate"]

        else:
            rate_dict["rate"] = INCREASE_COEFF * rate_dict["rate"]        
       

    def get_best_rate(self, paragraphs_texts, paragraphs_conts, names_set_copy):

        rate_dict = {}
        rate_dict["best_rate"] = 10000
        rate_dict["fin_rate"] = 0
        rate_dict["rate"] = 5

        for i in range(0, self.ShortRateIterations):
            self.summarize_chapter(paragraphs_texts, paragraphs_conts, rate_dict["rate"])
            self.SummaryParams.NamesSet = copy.deepcopy(names_set_copy)
            _, _, char_rate = self.ChapterRates.get_rates()
            self.reset_soup_state()
            self.tune_rate(char_rate, rate_dict)

        return rate_dict["fin_rate"]                       
                  

    def get_summary(self, paragraphs):
                                   
        paragraphs_texts = [x.text for x in paragraphs]
        paragraphs_conts = [x.prettify() for x in paragraphs]

        for i in range(0, len(paragraphs_texts)):
            if paragraphs_texts[i].startswith('*') or 'class="fnote"' in paragraphs_conts[i]:
                paragraphs_conts[i] = '<p></p>'

        names_set_copy = copy.deepcopy(self.SummaryParams.NamesSet)
        fin_rate = self.get_best_rate(paragraphs_texts, paragraphs_conts, names_set_copy)

        self.reset_soup_state()
        self.SummaryParams.NamesSet = copy.deepcopy(names_set_copy)
        self.summarize_chapter(paragraphs_texts, paragraphs_conts, fin_rate)
        self.OrigChapterLen[self.CurrSrc] = self.ChapterRates.LenCharsOriginal
        self.OrigLen += len("".join(paragraphs_texts))

        self.SummaryParams.NamesSet = collections.defaultdict(int)



    def get_paragraph_summary(self, paragraphs):

        if not paragraphs:
            return

        paragraphs_texts, paragraphs_conts = glue_dialogues(paragraphs)

        self.ChapterRates.reset()

        try:
            summary = generateParagraphSummaries(paragraphs_texts, paragraphs_conts,
                                                len_multiplier=len(paragraphs_texts) * self.DesiredParagraphRate,
                                                summary_params = self.SummaryParams)
        except ValueError:
            self.ChapterRates.update("".join(paragraphs_texts), "".join(paragraphs_texts))
            self.SummaryLen += len ("".join(paragraphs_texts))
            return

        summary = unglue_dialogues(summary)

        replace_old_paragraphs(paragraphs, summary)

        summary_texts = [bs4.BeautifulSoup(sentence).find('p').text for sentence in summary]
        self.ChapterRates.update("".join(summary_texts), "".join(paragraphs_texts))
        self.SummaryLen += len ("".join(summary_texts))


    def find_top_heading(self, paragraphs):

        prev_element = paragraphs[-1].find_previous(['h3', 'h2', 'h1'])

        if not prev_element:
            prev_element = paragraphs[0].find_previous(['h3', 'h2', 'h1'])

        if not prev_element:
            prev_element = self.CurrSoup.find(['h3', 'h2', 'h1'])

        return prev_element


    def append_chapter_tag(self, paragraphs):

        if self.CurrSrc == self.FrontPage:
            return

        if self.ChapterRates.LenParagraphsOriginal and paragraphs and self.CurrSrc and self.CurrSrc in self.OrigChapterLen:
            char_rate = min(float(self.ChapterRates.LenCharsSummary) / self.OrigChapterLen[self.CurrSrc], 1) 
            rate_mark = self.CurrSoup.new_tag('i')
            rate_mark.string = " (%.0f %s)" % (char_rate * 100, "%")
             
            prev_element = self.find_top_heading(paragraphs)

            if prev_element:
                old_mark = prev_element.find(lambda x: x.name == 'i' and '%' in x.text)
                if not old_mark:
                    prev_element.append(rate_mark)
                    print prev_element.text
                else:
                    _, _, char_rate = self.SigilChapterRates.get_rates()
                    rate_mark.string = " (%.0f %s)" % (char_rate * 100, "%")
                    old_mark.replace_with(rate_mark) # add merging here



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--epub', help='path to .epub', required=True)
    parser.add_argument('--lang', help='stopwords lang', required=True)
    parser.add_argument('--dst_epub', help='dst epub', required=True)
    parser.add_argument('--short_rate_iterations', type=int, help='shortening rate - a float', required=True)
    parser.add_argument('--subj_threshold', type=float, help='subj_threshold', default=0.3)
    parser.add_argument('--obj_threshold', type=float, help='obj_threshold', default=0.4)
    parser.add_argument('--desired_rate', type=float, help='desired abridging rate', default=0.3)
    parser.add_argument('--desired_paragraph_rate', type=float, help='desired paragraph abridging rate', default=0.3)
    parser.add_argument('--min_text_len', type=int, help='minimum text length in a sentence to get summarized', default=6)
    args = parser.parse_args()
    epub_summarizer = EpubSummarizer(args)
    epub_summarizer.create_epub()

