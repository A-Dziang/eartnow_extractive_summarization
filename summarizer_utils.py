﻿import re
import bs4
import nltk
from nltk.corpus import stopwords
from summarizer import segmentize, TAG_RE, QUOTES
import collections
import urllib2

class SummaryParams:
            
    def __init__(self, args):
        if ',' in args.lang:
            self.Lang = args.lang.split(',')
        else:
            self.Lang = [args.lang]
        self.SubjThreshold = args.subj_threshold
        self.ObjThreshold = args.obj_threshold
        self.NamesSet = collections.defaultdict(int)
        self.Chunker = None
        self.NamedEntities = None
        self.SentTokenizer = nltk.data.load('tokenizers/punkt/' + self.Lang[0] + '.pickle')


DEFAULT_NAMES_SET = ['human', 'men', 'women', 'boys', 'girls', 'wives', 'sisters',
                     'brothers', 'fathers', 'mothers', 'husbands', 'daughters',
                     'aunts', 'uncles', 'friends', 'cousins', 'nieces', 'sons',
                     'grandmothers', 'grandfathers', 'ancestors', 'humans',
                     'man', 'woman', 'boy', 'girl', 'wife', 'sister', 'brother',
                     'child', 'children', 'father', 'mother', 'husband', 'daughter',
                     'aunt', 'uncle', 'friend', 'cousin', 'niece', 'son',
                     'grandmother', 'ancestor', 'grandfather']


EDITORIAL_NOTE = {

    'english':(["This eBook follows the original text.", "This eBook is an exact transcription of the original text.", 'eBook', 'transcription'],
               "This eBook has been carefully abridged to %.0f %s of the original unabridged edition published by e-artnow:"),

    'german':(["Dieses eBuch folgt dem Originaltext."],
              "Quickie Klassiker sind sorgfältig gekürzte Versionen der Literatur-Klassiker. Die leserfreundlichen E-Books bewahren \
               gänzlich Sprache und Stil des Autors der Originalfassung, um der \"Essenz\" des Originals treu zu bleiben. Die Quickie \
               Klassiker sparen Ihnen nicht nur Zeit, sondern erhalten gleichzeitig noch die Authentizität und die Herrlichkeit dieser \
               literarischen Meisterstücke. Entdecken Sie wahre Lesefreude neu, indem Sie in die zeitlosen Geschichten der kultigen Autoren \
               eintauchen! Dieses E-Book wurde sorgfältig und angemessen auf %.0f %s der ungekürzten Originalversion von e-artnow gekürzt:")
    }

SPAN_RE = re.compile(u'<span.{1,50}span>', re.U) # any span with a special subclass

SIGIL_TOC_ID_RE = re.compile('[A-z_-]+\d+')
LIST_RE = re.compile('[0-9]+')

def looks_like_list_item(text):

    res = LIST_RE.search(text[0:20])
    if res:
        return True

    return False

# clearing away sigil markers
 
def clear_src(src):

    if '#' in src:
        if not SIGIL_TOC_ID_RE.match(src.split('#')[1]):
            print "ALARM!!! BAD ID!!!"
#            exit()
        return src.split('#')[0], src.split('#')[1]
 
    return src, ""

def replace_summarized_tag(paragraph, attrs, summary, curr_soup, curr_log):

    summary_text = " ".join(summary)
    p = bs4.BeautifulSoup(attrs + summary_text + "</p>").p.extract()
    curr_log.write(summary_text + '\n')

    try:
        old_p = curr_soup.find(lambda x: x.name == 'p' and
                               (x.text and unicode(paragraph) == unicode(x.text)
                               or x.string and unicode(paragraph) == unicode(x.string)))
        old_p.insert_after(p)
        old_p.decompose()

    except Exception as e:
        print e
        return False

    return True


def is_quoted(sentence):

    for quote in QUOTES:
        if quote in sentence:
            return True

    return False


        
def first_nontag_index(sentences, start_index):

    for index in range(start_index, len(sentences)):
        if not TAG_RE.match(sentences[index]):
            return index
 
    return None

def r_first_nontag_index(sentences, start_index):

    for index in range(start_index - 1, -1, -1):
        if not TAG_RE.match(sentences[index]):
            return index
 
    return None

def has_dash_or_question(text):

    semicolon_pos = text.rfind(":")
    if semicolon_pos != -1 and abs(len(text) - semicolon_pos) < 4:
        return True

    dash_pos = text.rfind("—")
    if dash_pos != -1 and abs(len(text) - dash_pos) < 4:
        return True

    question_pos = text.rfind("?")
 
    if question_pos != -1 and abs(len(text) - question_pos) < 5:
        return True
 
    return False


def flush_quotelist(quotelist, contlist, paragraphs_texts, paragraphs_conts):

    if quotelist:

        paragraphs_texts.append(" ".join(quotelist))

        if len(contlist) > 1:

            paragraphs_conts.append(tuple(contlist))

        else:

            paragraphs_conts.append(contlist[0])

        del quotelist[:]
        del contlist[:]

def continue_dialogue_chunk(paragraph_text, prev_question_or_dash, next_question_or_dash, quotelist):

    if ((is_quoted(paragraph_text) or prev_question_or_dash) and 
       (len(quotelist) < 5 or prev_question_or_dash) and
       not has_dash_or_question(paragraph_text) and not next_question_or_dash):

        return True

    return False

def append_text_and_content(quotelist, contlist, paragraph):

    quotelist.append(paragraph.text)
    contlist.append(paragraph.prettify().replace('\n', ' '))

def dialogue_process_paragraph(paragraphs, index, quotelist, contlist,
                               paragraphs_texts, paragraphs_conts, prev_question_or_dash, next_question_or_dash):

    paragraph = paragraphs[index]              

    if continue_dialogue_chunk(paragraph.text, prev_question_or_dash, next_question_or_dash, quotelist):

        append_text_and_content(quotelist, contlist, paragraph)

    else:

        flush_quotelist(quotelist, contlist, paragraphs_texts, paragraphs_conts)

        if has_dash_or_question(paragraph.text) or next_question_or_dash:
            append_text_and_content(quotelist, contlist, paragraph)
        else:
            append_text_and_content(paragraphs_texts, paragraphs_conts, paragraph)


def glue_dialogues(paragraphs):

    paragraphs_texts = []
    paragraphs_conts = [] 

    quotelist = []
    contlist = []

    prev_question_or_dash = False
    next_question_or_dash = False

    for index in range(0, len(paragraphs)):

        if index < len(paragraphs) - 1:

            next_question_or_dash = has_dash_or_question(paragraphs[index + 1].text)

        else:

            next_question_or_dash = False

        dialogue_process_paragraph(paragraphs, index, quotelist, contlist,
                                   paragraphs_texts, paragraphs_conts,
                                   prev_question_or_dash, next_question_or_dash)
        
        prev_question_or_dash = has_dash_or_question(paragraphs[index].text)

    flush_quotelist(quotelist, contlist, paragraphs_texts, paragraphs_conts)                                   
    return paragraphs_texts, paragraphs_conts


def replace_old_paragraphs(paragraphs, summary):

    last_p = paragraphs[0]

    for old_p in paragraphs[1:]:
        old_p.decompose()

    for index in range(len(summary) - 1, 0, -1):
        p = bs4.BeautifulSoup(summary[index]).find('p')
        p = p.extract()
        last_p.insert_after(p)


def unglue_dialogues(summary):

    flattened_summary = []

    for x in summary:

        if isinstance(x, tuple):

            flattened_summary.extend(list(x))

        else:

            flattened_summary.append(x)

    return flattened_summary

        



def is_p_and_has_text(tag):

    return tag.name == 'p' and tag.find('p') == None        

def has_no_sigil_marker(element):

    return ('id' not in element.attrs or not element['id'] or
            (not element['id'].startswith('sigil') and
            not element['id'].startswith('e_art') and
            not element['id'].startswith('eartnow_') and
            not element['id'].startswith('e_artnow') and
            not element['id'].startswith('e-art_id') and
            not element['id'].startswith('filepos')))   

def descend_tree(the_tag, editorial_note):

    for x in the_tag.children:

        if not isinstance(x, bs4.NavigableString) and has_editorial_note(editorial_note, x.text):
            return descend_tree(x, editorial_note)

    return the_tag


def has_editorial_note(editorial_note, text):

    for note_version in editorial_note:

        if note_version in text:

            return True

    return False


def process_lang(front_soup, editorial_note, new_editorial_note):

    editorial_span = front_soup.find(lambda x: x.name == 'span' and                                            
                                     has_editorial_note(editorial_note, x.text))
    if not editorial_span:
        editorial_span = front_soup.find(lambda x: x.name == 'div' and
                                         has_editorial_note(editorial_note, x.text))

    saved_editorial_span = editorial_span
    editorial_span = descend_tree(editorial_span, editorial_note)

    try:

        editorial_span.string.replace_with(new_editorial_note)

    except:

        for content in editorial_span.contents:

            if has_editorial_note(editorial_note, content):

               content.replace_with(new_editorial_note)

    return saved_editorial_span


def get_title(front_soup):

    title = front_soup.new_tag('div')
    title.string = front_soup.find('h1').text
    return title


def get_isbn(front_soup):
 
    isbn_div = front_soup.find(lambda x: x.name == 'span' and 'ISBN' in x.text)

    if not isbn_div:
        isbn_div = front_soup.find(lambda x: x.name == 'div' and 'ISBN' in x.text)

    isbn = front_soup.new_tag('div')
    isbn.string = isbn_div.string
 
    return isbn


def get_image(title_soup):

    image_div = title_soup.new_tag('div') 
    image = title_soup.find('svg').extract()
    image['xmlns'] = "http://www.w3.org/2000/svg"
    image['xmlns:xlink'] = "http://www.w3.org/1999/xlink"
    image.image['xlink:href'] = image.image['href']

    del image.image.attrs['href']
    image_div.append(image)

    return image_div


def filter_tags(src):
    return [x for x in src if TAG_RE.match(x) == None]


def add_iframes(path, front_soup):

    iframes = front_soup.find_all('iframe')
    for iframe in iframes:
        new_soup = bs4.BeautifulSoup(open(path + iframe.attrs['src']), 'lxml')
        iframe.replace_with(new_soup)   

def process_first_pages(path, first_page, title_page, langs, final_rate):

    front_soup = bs4.BeautifulSoup(open(path + first_page), 'xml')
    add_iframes(path, front_soup)
    title_soup = bs4.BeautifulSoup(open(path + title_page), 'xml')
    saved_editorial_span = None

    for lang in langs:
        try:
            saved_editorial_span = process_lang(front_soup, EDITORIAL_NOTE[lang][0],
                                                EDITORIAL_NOTE[lang][1] % (final_rate * 100, "%"))
        except:
            pass


    while len(saved_editorial_span.find_all('b') + saved_editorial_span.find_all('div', class_ ='calibre1')) < len(langs): 
        saved_editorial_span = saved_editorial_span.parent

    new_elements = [
                        get_title(front_soup),
                        get_isbn(front_soup),
                        get_image(title_soup)
                   ]

    for new_element in new_elements:
        saved_editorial_span.append(new_element)

    open(path + first_page, 'w').write(front_soup.prettify())    


def get_first_pages(path):

    TocPage = None
    FrontPage = None
    TitlePage = None

    content_soup = bs4.BeautifulSoup(open(path + 'content.opf'))
    important_pages = content_soup.spine.find_all('itemref')

    for page in important_pages:

        idref = page['idref']

        if 'toc.' in idref or 'content.' in idref or 'contents.' in idref or 'TOC.' in idref:
            TocPage = 'Text/' + idref
            if 'xhtml' not in idref:
                TocPage += '.xhtml'

        if 'front' in idref:
            FrontPage = 'Text/' + idref
            if 'xhtml' not in idref:
                FrontPage += '.xhtml'

        if 'title' in idref or 'cover' in idref:
            TitlePage = 'Text/' + idref
            if 'xhtml' not in idref:
                TitlePage += '.xhtml'

    return TocPage, FrontPage, TitlePage
