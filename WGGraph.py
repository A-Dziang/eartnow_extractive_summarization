#!/usr/bin/python
# -*- coding: utf-8 -*-

import codecs
import re, igraph
from pattern.text import parsetree 
import numpy as np
import networkx as nx
from random import shuffle
from sklearn.metrics.pairwise import linear_kernel
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from pulp import * #LpAffineExpression, LpConstraint, LpConstraintVar, lpSum
import Stemmer
from sklearn.metrics.pairwise import cosine_similarity
import summarizer
from nltk.tokenize import WordPunctTokenizer

english_stemmer = Stemmer.Stemmer('en')
class StemmedTfidfVectorizer(CountVectorizer):
    def build_analyzer(self):
        analyzer = super(CountVectorizer, self).build_analyzer()
        return lambda doc: english_stemmer.stemWords(analyzer(doc))


def getredundantComponents(sentences):
#    window_size=4
    introList=[]
    midlist=[]
    endlist=[]
    
    for sent in sentences:
        words = WordPunctTokenizer().tokenize(sent)
        length_sent = len(words)

        window_size = max(4, length_sent // 3 + 1)
        
        f_point = (length_sent)//3
        m_point = (length_sent)//2
        index_span = window_size//2
        intro = ' '.join(word for word in words[0 : window_size])
        mid = ' '.join(word for word in words[m_point-index_span : m_point+index_span])
        end = ' '.join(word for word in words[-window_size:])
        introList.append(intro)
        midlist.append(mid)
        endlist.append(end)
    return introList, midlist, endlist


def getCosineSimilarityMatrix(docs, stopwords):
    bow_matrix = StemmedTfidfVectorizer(stop_words=stopwords, min_df=1).fit_transform(docs)
    normalized = TfidfTransformer().fit_transform(bow_matrix)
    cosine_similarity_matrix = (normalized * normalized.T).A
    return cosine_similarity_matrix


def get_all_indices(i, cosine_similarity_matrix, cosine_similarity_matrix_i,
                     cosine_similarity_matrix_m, cosine_similarity_matrix_e, sbjthreshold, objthreshold):
    i_indices=np.where(cosine_similarity_matrix_i[i,:]>= objthreshold)[0]
    m_indices=np.where(cosine_similarity_matrix_m[i,:]>= objthreshold)[0]
    e_indices=np.where(cosine_similarity_matrix_e[i,:]>= objthreshold)[0]
    s_indices=np.where(cosine_similarity_matrix[i,:]>= sbjthreshold)[0]    
    all_indices=np.concatenate((i_indices, m_indices))
    all_indices=np.concatenate((all_indices, e_indices))
    all_indices=np.unique(all_indices)
    all_indices=np.concatenate((all_indices, s_indices))
    all_indices=np.unique(all_indices).tolist()
    return all_indices


def fill_visitedlist(varlist, docs, stopwords, cosine_similarity_matrix, sbjthreshold, objthreshold, m):

    # Splitting a sentence into three parts
    ilist, mlist, elist=getredundantComponents(docs)

    cosine_similarity_matrix_i = getCosineSimilarityMatrix(ilist, stopwords)
    cosine_similarity_matrix_m = getCosineSimilarityMatrix(mlist, stopwords)
    cosine_similarity_matrix_e = getCosineSimilarityMatrix(elist, stopwords)
    

    visitedlist=[]
    for i in xrange(len(varlist)):
        all_indices=get_all_indices(i, cosine_similarity_matrix, cosine_similarity_matrix_i,
                     cosine_similarity_matrix_m, cosine_similarity_matrix_e, sbjthreshold, objthreshold)
        for j in all_indices:
            if i==j:
                continue
            
            if j==len(varlist):
                continue

            if (i, j) not in visitedlist:
                visitedlist.append((i,j))
                m+=varlist[i] + varlist[j] <=1.0, "constraint_facts_svo_"+str(i)+"_"+varlist[i].name+"_"+varlist[j].name 

        completelist=[]
        completelist.extend(mlist)
        completelist.append(ilist[i])     
        lastelement=len(completelist)-1
   
        cosine_similarity_matrix_ilist = getCosineSimilarityMatrix(completelist, stopwords)

        r_indices=np.where(cosine_similarity_matrix_ilist[lastelement,:]>= objthreshold)[0]
        oth_indices=r_indices
        completelist=[]
        completelist.extend(elist)
        completelist.append(ilist[i])

        cosine_similarity_matrix_ilist = getCosineSimilarityMatrix(completelist, stopwords)

        r_indices=np.where(cosine_similarity_matrix_ilist[lastelement,:]>= objthreshold)[0]
        oth_indices=np.concatenate((oth_indices, r_indices)) 
         
        completelist=[]
        completelist.extend(mlist)
        completelist.append(elist[i])
        cosine_similarity_matrix_ilist = getCosineSimilarityMatrix(completelist, stopwords)
        r_indices=np.where(cosine_similarity_matrix_ilist[lastelement,:]>= objthreshold)[0]

        oth_indices=np.concatenate((oth_indices, r_indices)) 
        
        oth_indices=np.unique(oth_indices).tolist()
        for j in oth_indices:
            if i==j:
                continue
            
            if j==len(varlist):
                continue
            if (i, j) not in visitedlist:
                visitedlist.append((i,j))
                m+=varlist[i] + varlist[j] <=1.0, "constraint_facts_comps_"+str(i)+"_"+varlist[i].name+"_"+varlist[j].name
 
    return visitedlist
    
def solveILPFactBased(groupedList, stopwords, ranker, l_max=100, sbjthreshold=0.3, objthreshold=0.4):
    
    if len(groupedList) == 0:
        return
    
    m = LpProblem("mip1", LpMaximize)
    
    docs=[]
    for element in groupedList:
        docs.append(element)

    # Full sentence cosine sim comparison
    cosine_similarity_matrix = getCosineSimilarityMatrix(docs, stopwords)


    txtRankScores=[]
    if ranker == "Centroid":
        txtRankScores=summarizer.getScores(docs)
    if ranker == "textrank":
        sources, targets = cosine_similarity_matrix.nonzero()
        similarity_igraph = igraph.Graph(zip(sources, targets), directed=True)
        txtRankScores = igraph.Graph.pagerank(similarity_igraph)
 
    
    varlist=[]        
    for i in xrange(len(docs)):
            var=LpVariable("var_"+str(i),cat=LpBinary)
            varlist.append(var)
    
    m += lpSum([txtRankScores[i]*varlist[i] for i in xrange(len(txtRankScores))]), "Obj function" 
    
    visitedlist = fill_visitedlist(varlist, docs, stopwords, cosine_similarity_matrix, sbjthreshold, objthreshold, m)

    gen_lengths=[]
    for i in xrange(len(txtRankScores)):
        words=[x for x in docs[i].split(" ") if x]
        count=0
        for word in words:
            if word[0].isalpha() or word[0].isdigit():
                count+=1
        gen_lengths.append(count)

    m += lpSum([varlist[i]*gen_lengths[i] for i in xrange(len(txtRankScores))]) <= l_max, "length of summary"  

    m.solve()
    solutionList=[] 
    
    for v in m.variables():
        if v.varValue == 1.0:
            indexVar=v.name.split("_")[1]
            solutionList.append(docs[int(indexVar)])
    return solutionList


