﻿#coding: utf-8
from __future__ import print_function

import glob
import re
import sys
import math
from collections import Counter
import nltk.data
from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktParameters

TAG_RE = re.compile(u' *(\<[^>]*\>) *', re.U|re.M)
SPLIT_TAG_RE = re.compile(u' *(\<[/]*(?:(?:span)|(?:p))(?:[^>]*)*\>) *', re.U|re.M)
QUOTES = [u'“', '\'', '"', u'”', u'»', u'«']

ABBREV_DICT = {
    'english': ['dr', 'vs', 'mr', 'mrs', 'prof', 'inc', 'i.e'],
    'german': ['z', 'a', 'bzw', 'geb', 'n', 'chr', 'uzw',
               'i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii',
               'viii', 'ix', 'x', 'xi', 'xii', 'x', 'xi',
               'xii', 'xiii', 'xiv', 'xv', 'xvi', 'xvii',
               'xviii', 'xix', 'xx', 'xxi']
}
    
   
def glue_sentences(pre_sentences):

    unglued_sentences = []

    for pre_sentence in pre_sentences:
        unglued_sentences.extend([x.strip() for x in SPLIT_TAG_RE.split(pre_sentence) if x and x.strip()])

    prev = ""
    sentences = []

    for pre_sentence in unglued_sentences:

        if pre_sentence[0] == pre_sentence[0].upper():

            if prev:
                sentences.append(prev.strip())
            prev = pre_sentence

        else:
            prev += (" " + pre_sentence)

    if prev:
        sentences.append(prev.strip())

    return sentences


def segmentize(text, lang):

    sent_detector = nltk.data.load('tokenizers/punkt/' + lang[0] + '.pickle')

    for quote in QUOTES:
        text = text.replace('.' + quote, '. ' + quote)
        text = text.replace(quote + '.', quote + ' .')

    for lng in lang:

        if lng in ABBREV_DICT:
            sent_detector._params.abbrev_types.update(ABBREV_DICT[lng])

    pre_sentences = sent_detector.tokenize(text)

    return glue_sentences(pre_sentences)


def words(text):
    return [w.lower() for w in re.findall(r'[^\s.:,;!()?/\'"]+', text)]


def compute_idf(sentences):

    idf = {}
    filecount = 0.0

    for sent in sentences:
        filecount += 1

        for w in set(words(sent)):
            idf[w] = idf.get(w, 0) + 1

    for w in idf.keys():
        idf[w] = math.log(filecount / idf[w])

    return idf


def compute_tf(text):

    counter = Counter(words(text))
    total_words = sum(c for w, c in counter.items())
    tf = dict((w, float(c)/total_words) for w, c in counter.items())
    return tf


def cosinesim(idf, text1, text2):

    tf1 = compute_tf(text1)
    words1 = [w for w in tf1.keys()]
    tf2 = compute_tf(text2)
    words2 = [w for w in tf2.keys()]
    dotprod = sum(tf1.get(w, 0) * tf2.get(w, 0) * (idf[w] ** 2) for w in
            set(words1 + words2))
    norm1 = math.sqrt(sum((tf1[w] * idf[w])**2 for w in words1))
    norm2 = math.sqrt(sum((tf2[w] * idf[w])**2 for w in words2))
    return dotprod / (norm1 * norm2)


def rank(idf, sentences):

    scount = len(sentences)
    sim_matrix = [[0] * scount for i in range(scount)]

    for i in range(scount-1):

        for j in range(i+1, scount):
            sim_matrix[i][j] = cosinesim(idf, sentences[i], sentences[j])
            sim_matrix[j][i] = sim_matrix[i][j]

    ranking = []

    for spos in range(scount):
        avg_sim = sum(sim_matrix[spos]) / scount
        ranking.append(avg_sim)

    #ranking.sort(key=lambda t: t[1], reverse=True)
    return ranking
    

def getScores(sentences):
    idf = compute_idf(sentences)
    ranking = rank(idf, sentences)
    return ranking
    
