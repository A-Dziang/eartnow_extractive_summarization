﻿import subprocess
import custom_ner
from summarizer_utils import *



def run_shell_command(command, print_output = False):

    output = subprocess.check_output([command], shell=True)

    if print_output:
        print output

    return output


def make_folder(folder_name):

    run_shell_command('mkdir %s;' % folder_name)


def remove_file_or_folder(name):

    run_shell_command('rm -rf %s;' % name)


def copy_file(src, dst):

    run_shell_command('cp %s %s;' % (src, dst))


def unzip_to_folder(unzip_folder, source_archive, dest_epub):

    make_folder(unzip_folder)
    run_shell_command('cd %s; cp ../%s .; unzip -o %s;' % (unzip_folder, source_archive, source_archive))
    remove_file_or_folder(dest_epub)


def unzip_from_epub(unzip_folder, source_epub, source_archive, dest_epub):

    run_shell_command('cp %s ./%s/%s; cd %s; unzip -o %s;' % (source_epub,
                                                              unzip_folder,
                                                              source_archive,
                                                              unzip_folder,
                                                              source_archive))
    remove_file_or_folder(dest_epub)


def zip_content_together(tmp_folder, dst_zip):


    run_shell_command('cd %s;\
                       zip -X ../%s mimetype;\
                       zip -rX ../%s META-INF;\
                       zip -rX ../%s OEBPS;' % (tmp_folder, dst_zip, dst_zip, dst_zip))



class EpubFileHandler:


    def __init__(self, args):

        self.SrcEpub = args.epub
        self.DstEpub = args.dst_epub
        self.Chunker = None
        self.SrcZip = self.SrcEpub.replace('.epub', '.zip')
        self.DstZip = self.DstEpub.replace('.epub', '.zip')
        self.TmpFolder = 'tmp'
        self.BookCorpus = "%s.book_corpus" % self.SrcEpub
        self.GetBookCorpus = 'python check_final_rates.py --epub1 %s --epub2 %s --extract_text=True > %s' % (self.SrcEpub,
                                                                                                             self.SrcEpub, self.BookCorpus)
        self.Text = ""
        self.NamedEntities = []


    def convert_html_to_xhtml(self):

        files = run_shell_command("find %s -type f" % self.TmpFolder).split('\n')
        print files
        tmp_file = 'tmp_file'
        text_files = [x for x in files if '.epub' not in x and '.zip' not in x and x]
        for file in text_files:
            run_shell_command("cat %s | sed -e 's/\.html/.xhtml/g' > %s" % (file, tmp_file))
            run_shell_command("cp %s %s" % (tmp_file, file.replace('.html', '.xhtml')))
            self.Text += TAG_RE.sub(' ', open(file).read()).replace('\n', '')
            if '.html' in file:
                run_shell_command("rm %s" % file)


    def convert_and_unpack_epub(self):

        run_shell_command(self.GetBookCorpus)
        chunker, named_entities = custom_ner.get_chunker(self.BookCorpus)
        self.Chunker = chunker
        self.NamedEntities = named_entities
        self.unpack_epub()
        # repacking the converted epub back and unpacking it back                           
        zip_content_together(self.TmpFolder, self.DstZip)
        copy_file(self.DstZip, "../%s" % self.SrcZip)
        remove_file_or_folder(self.TmpFolder)
        unzip_to_folder(self.TmpFolder, self.SrcZip, self.DstEpub)

    def unpack_epub(self):
        make_folder(self.TmpFolder)
        unzip_from_epub(self.TmpFolder, self.SrcEpub, self.SrcZip, self.DstEpub)
        self.convert_html_to_xhtml()

    def pack_epub(self):

        zip_content_together(self.TmpFolder, self.DstZip)
        copy_file(self.DstZip, self.DstEpub)
        remove_file_or_folder(self.DstZip)
        remove_file_or_folder(self.TmpFolder)

